config_variables = {

        "attributes": {     # Format: "Pretty name": "Data file heading"
                "Shoot": "SHO",
                "Tackle": "TCK",
                "Pass": "PAS"
                },
        "sleep_secs": 3,    # The length of the sleep period between each
                            # action (in seconds)
        "data_location": "data.csv"     # Path to the data file relative to the
                                        # PyopPyumps.py file (either absolute
                                        # or relative)
}
