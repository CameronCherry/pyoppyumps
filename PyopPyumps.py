# import numpy as np
import os.path as opath
import pandas as pd
import random
import time
import config as cfg

# Import variable values from config.py
attribs_dict = cfg.config_variables["attributes"]
sleep_secs = cfg.config_variables["sleep_secs"]
data_location = cfg.config_variables["data_location"]

attribs_longhand = list(attribs_dict)
attribs_shorthand = [attribs_dict[x].upper() for x in attribs_longhand]
num_stats = len(attribs_dict)

pack = range(num_cards)
df = pd.read_csv(data_location)
num_cards = len(df.index)

names_data = list(df['NAME'])

cpu_hand = []
player1_hand = []


def check_setup_valid():
    all_ok = False
    types_ok = isinstance(num_cards, int) and isinstance(num_stats, int) and\
        isinstance(sleep_secs, int) and isinstance(data_location, str)
    data_loc_ok = opath.isfile(data_location) and\
        data_location.lower().endswith('.csv')
    all_ok = types_ok and data_loc_ok
    return all_ok


class Card:
    def __init__(self, name):
        self.name = name
        self.data_index = names_data.index(self.name)
        data_row = list(df.iloc[self.data_index])
        self.params = [param for param in data_row[1:]]


def shuffle_cards(pack_old):
    new_order = random.sample(range(num_cards), num_cards)
    pack_new = [pack_old[elem] for elem in new_order]
    return pack_new


def deal_cards(pack):
    global cpu_hand
    global player1_hand
    pack = shuffle_cards(pack)
    num_hands = 2
    cpu_hand_size = round(num_cards / num_hands)
    cpu_hand = pack[:cpu_hand_size].copy()
    player1_hand = pack[cpu_hand_size:].copy()


def cpu_turn(cpu_card, player1_card):
    global cpu_hand
    global player1_hand
    rnd = random.randint(0, num_stats - 1)
    chosen_attrib = attribs_longhand[rnd]
    print("CPU is thinking...")
    dramatic_pause(for_suspense=True)
    print(f"CPU played {chosen_attrib}...")

    cpu_val = cpu_card.params[attribs_longhand.index(chosen_attrib)]
    player1_val = player1_card.params[attribs_longhand.index(
        chosen_attrib)]

    if cpu_val == player1_val:
        winner = None
        print(f"Winner is no-one!")
        cpu_hand.append(cpu_hand.pop(0))
        player1_hand.append(player1_hand.pop(0))
    elif cpu_val > player1_val:
        winner = "CPU"
        cpu_hand.append(player1_card)
        player1_hand.remove(player1_card)
        print(f"Winner is {winner}!")
        cpu_hand.append(cpu_hand.pop(0))
    else:
        winner = "Player1"
        player1_hand.append(cpu_card)
        cpu_hand.remove(cpu_card)
        print(f"Winner is {winner}!")
        player1_hand.append(player1_hand.pop(0))
    return winner


def player1_turn(player1_card, cpu_card):
    global cpu_hand
    global player1_hand
    chosen_attrib = None

    valid_inputs = [x.lower() for x in attribs_longhand]

    while True:
        chosen_attrib = input(
            f"Select which stat you would like to play.\n\
            {attribs_longhand}: ").lower()

        if chosen_attrib not in valid_inputs:
            print("Invalid input.  Please try again.")
            print(f"Try one of: {valid_inputs}.")
        else:
            break

    chosen_attrib = chosen_attrib.title()

    print(f"You played {chosen_attrib}.")
    cpu_val = cpu_card.params[attribs_longhand.index(chosen_attrib)]
    player1_val = player1_card.params[attribs_longhand.index(chosen_attrib)]

    if cpu_val == player1_val:
        winner = None
        print(f"It's a tie!")
        cpu_hand.append(cpu_hand.pop(0))
        player1_hand.append(player1_hand.pop(0))
    elif cpu_val > player1_val:
        winner = "CPU"
        cpu_hand.append(player1_card)
        player1_hand.remove(player1_card)
        print(f"Winner is {winner}!")
        cpu_hand.append(cpu_hand.pop(0))
    else:
        winner = "Player1"
        player1_hand.append(cpu_card)
        cpu_hand.remove(cpu_card)
        print(f"Winner is {winner}!")
        player1_hand.append(player1_hand.pop(0))
    return winner


def dramatic_pause(for_suspense=False):
    if for_suspense:
        pause_duration = sleep_secs * 2
    else:
        pause_duration = sleep_secs

    time.sleep(pause_duration)


def print_names_in_hand(hand):
    for crd in hand:
        print(f" - {crd.name}")


def print_card(crd):
    global attribs_shorthand
    print(f"- Name: {crd.name}")
    for attrib in crd.params:
        print(f"- {attribs_shorthand[crd.params.index(attrib)]}: {attrib}")


def inform_user():
    global player1_hand
    print("Your hand is:")
    print_names_in_hand(player1_hand)
    dramatic_pause()
    print("Your current card is:")
    print_card(player1_hand[0])


if __name__ == "__main__":

    if not check_setup_valid():
        print("The configuration file looks like it might have some issues.")
        print("Check the README.md file to ensure you have valid inputs.")
    else:
        playing = True
        while playing:
            pack = []
            for nom in names_data:
                next_card = Card(name=nom)
                pack.append(next_card)

            print("Dealing cards...")
            deal_cards(pack)
            print("Player1:")
            print_names_in_hand(player1_hand)
            print("CPU:")
            print_names_in_hand(cpu_hand)

            turn_victor = None

            while min(len(player1_hand), len(cpu_hand)) > 0:

                while turn_victor != "Player1" and min(len(player1_hand),
                                                       len(cpu_hand)) > 0:
                    inform_user()
                    turn_victor = cpu_turn(cpu_hand[0], player1_hand[0])
                    dramatic_pause()

                while turn_victor != "CPU" and min(len(player1_hand),
                                                   len(cpu_hand)) > 0:
                    inform_user()
                    turn_victor = player1_turn(player1_hand[0], cpu_hand[0])
                    dramatic_pause()

            if len(player1_hand) == 0:
                dramatic_pause()
                print("We have a winner!  CPU beat you fair and square!")
            elif len(cpu_hand) == 0:
                dramatic_pause()
                print("We have a winner!  You're a worthy victor!")
            else:
                print("Looks like something went wrong... sorry.")
                break

            play_again = input("Would you like to play again? (Y/n)")
            playing = (play_again == "Y")

        print("Quitting game...")
