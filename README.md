# PyopPyumps

## About

PyopPyumps (pronounced _"PyopPyumps"_) is a card game for two players.
Players take turns comparing card attributes.

## Example

Cards look like the following:

| Marcus Rashford |      |
| :---            | :--- |
| SHO:            |  18  |
| TCK:            |   4  |
| PAS:            |  14  |

A player can see their own card but not their opponent's.
On their turn, they choose which attribute they believe is most powerful.

Here, the player might choose the SHO (shoot) attribute to give themselves the
best possible chance of winning.
